const Course = require("../models/Course");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body.
	2. Save the new User to the database.
*/

module.exports.addCourse = (reqBody) =>{

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})

	return newCourse.save().then((course, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Retrieve all courses
/*
	Step:
	1. Retrieve all the courses from the database.
*/

module.exports.getAllCourses = () =>{
	return Course.find({}).then(result => result);
}

// Retrieve all active courses
/*
	Step:
	1. Retrieve all the courses from the database with the property of "isActive" to true. 
*/
module.exports.getAllActive = () =>{
	return Course.find({isActive: true}).then(result => result);
}

// Retrieve a specific course
/*
	Step:
	1. Retrieve a specific course that matches the courseID provided from the URL.
*/
module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result => result);
}

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body.
	2. Find and update the course using the findById/courseId retrieved from the request params or URL property and the variable "updatedCourse" containing the information from the request body.
*/

module.exports.updateCourse = (courseId, reqBody) => {
	// Specify the fields to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	}

	// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)

	return Course.findByIdAndUpdate(courseId, updatedCourse).then((courseUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Archive a course
/*
	Mini Activity:
	1. Create an archiveCourse function that will change the status of an active course to inactive using the "isActive" property.
	2. Return true, if the course is set to inactive, and false if encountered an error.
*/

// module.exports.archiveCourse = (courseId, reqBody) =>{
// 	return Course.findByIdAndUpdate(courseId, {isActive: true}).then((reqBody, error) =>{
// 		if(reqBody){
// 			return false;
// 		}
// 		else{
// 			return true;
// 		}
// 	})
// }

module.exports.archiveCourse = (courseId) =>{
	let updateActiveField = {
		isActive : false
	}
	return Course.findByIdAndUpdate(courseId, updateActiveField).then((isActive, error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}